#include <iostream>
#include <dirent.h>
#include <fstream>
#include <sys/stat.h>
#include "ImageCategory.h"
#include "ARFFUtils.h"

#define BINS 64

int getcategories(std::string, std::vector<ImageCategory>&);
void writeARFF(std::string, std::vector<ImageCategory>&);

int main(int argc, char** argv) {
    const std::string root_dir = std::getenv("HOME") + std::string("/Desktop/project-images/");
    const std::string arff_path = root_dir + "test" + std::to_string(BINS) + ".arff";

    Image::setNumBins(BINS);

    // Delegate image processing tasks to category objects
    std::vector<ImageCategory> cats;
    getcategories(root_dir, cats);

    for (int i = 0; i < cats.size(); ++i) {
        cats[i].getImages();
        cats[i].getRGBdata();
    }

    // Compose ARFF file
    writeARFF(arff_path, cats);

    return 0;
}

void writeARFF(std::string path, std::vector<ImageCategory>& cats) {
    arff::WriteArff wa(path);

    // set relation type
    wa.setRelation("NormalizedColorHistogram_" + std::to_string(BINS) + "Bins");

    // set and enumerate nominal type (image categories)
    wa.addNominalType("category", "{piano, kangaroo, strawberry, sunflower, airplane, face, leopard}");

    // add attributes
    for (int i = 0; i < BINS; ++i)
        wa.addAttribute("bin" + std::to_string(i + 1), arff::NUMERIC);
    wa.addAttribute("category", arff::NOMINAL);

    // add data
    for (int i = 0; i < cats.size(); ++i)
        wa.addData(cats[i].getARFFStrings());

    wa.close();
}

int getcategories(std::string path, std::vector<ImageCategory>& cats) {
    struct dirent *ent;
    DIR *dp = opendir(path.c_str());

    if (dp == NULL)
        return -1;

    // Load categories from folder names in project-image directory
    while ((ent = readdir(dp))) {
        std::string name = ent->d_name;
        struct stat st;
        lstat((path + name).c_str(), &st);

        // Make sure current entry is a directory
        if(S_ISDIR(st.st_mode) && name[0] != '.') {
            cats.push_back(ImageCategory(name, path));
        }
    }

    closedir(dp);
    return  0;
}