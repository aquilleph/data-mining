/** Mining -- Image.h
 * Created by Aquilla Sherrock on 01 December, 2014.
 * Copyright (c) 2014 Insignificant Tech. All rights reserved.
 */

#ifndef __Image_H_
#define __Image_H_

#include <iomanip>
#include <sstream>
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui_c.h>

class Image {
private:
    static const short MAX_VAL;
    static unsigned short BIN_COUNT;

    cv::Mat image;
    int* bins;
    double* normalized;
    short precision;

    std::string category;
    std::string name;
    std::string path;

    int rows;
    int cols;

    void placeInBins(cv::Vec3b pixel);

public:
    Image(std::string name, std::string category_path, std::string category);

    static void setNumBins(const unsigned short count);
    void getColorFrequencies();
    std::string getARFFString();
};

#endif /* defined __Image_H_ */