#include "Image.h"

const short Image::MAX_VAL = 256;
unsigned short Image::BIN_COUNT = 64;

Image::Image(std::string name, std::string category_path, std::string category) : name(name), category(category) {
    path = category_path + name;
    image = cv::imread(path, 1);

    bins = (int*) calloc((size_t) BIN_COUNT, sizeof(int));
    normalized = new double[BIN_COUNT];

    if (!image.data)
        throw std::runtime_error("Could not find image at " + path);

    precision =  MAX_VAL / (short) std::cbrt(BIN_COUNT);
    rows = image.rows;
    cols = image.cols;
}

// Inspect each pixel of image and extract RGB values
void Image::getColorFrequencies() {
    for (short i = 0; i < image.rows; i++) {
        for (short j = 0; j < image.cols; j++) {
            cv::Vec3b pixel = image.at<cv::Vec3b>(i, j);
            placeInBins(pixel);
        }
    }

    // get normalized frequencies of relative values
    for (short i = 0; i < BIN_COUNT; ++i)
        normalized[i] = bins[i] / (double) (rows * cols);
}

void Image::placeInBins(cv::Vec3b pixel) {
    uchar b = pixel[0];
    uchar g = pixel[1];
    uchar r = pixel[2];

    // Number of values to which each color can be quantized.
    short steps = MAX_VAL / precision;

    // Iterate through Red value ranges
    for (short i = 1; i <= steps; ++i) {
        if (r >= (i-1)*precision && r < i*precision)
        {
            // Iterate through Green value ranges
            for (short j = 1; j <= steps; ++j) {
                if (g >= (j-1)*precision && g < j*precision)
                {
                    // Iterate through Blue value ranges
                    for (short k = 1; k <= steps; ++k) {
                        if (b >= (k-1)*precision && b < k*precision)
                        {
                            /* Determine bin index by converting loop iterators into:
                             *  binary (8 bins)
                             *  hexideximal (64 bins)
                             *  tetrasexagesimal (512 bins) */
                            short index = std::pow(steps, 2) * (i-1) + steps * (j-1) + k-1;
                            bins[index] ++;
                        }
                    }
                }
            }
        }
    }
}

std::string Image::getARFFString() {
    std::ostringstream oss;
    for (short i = 0; i < BIN_COUNT; ++i)
        oss << normalized[i] << ", ";

    oss << category;
    return oss.str();
}

void Image::setNumBins(const unsigned short count) {
    Image::BIN_COUNT = count;
}