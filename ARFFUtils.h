/** Mining -- ARFFUtils.h
 * Created by Aquilla Sherrock on 02 December, 2014.
 * Copyright (c) 2014 Insignificant Tech. All rights reserved.
 */

#ifndef __ARFFUtils_H_
#define __ARFFUtils_H_

#include <iostream>
#include <fstream>
#include <map>

namespace arff {
    class WriteArff;
    class ReadArff;

    enum AttrType {
        NUMERIC,
        NOMINAL,
        STRING,
        DATE,
    };
}

class arff::WriteArff {
public:
    WriteArff(std::string path);
    ~WriteArff();

    void close();
    void addNominalType(std::string name, std::string values);
    void setRelation(std::string relation);
    void addAttribute(std::string attribute, AttrType type);
    void addData(std::string data);
private:
    std::ofstream ofs;
    std::string path;
    std::map<std::string, std::string> nominals;
    bool isdata;

    std::string getAttrTypeString(AttrType type, std::string nominal);
};

class arff::ReadArff {

};

#endif /* defined __ARFFUtils_H_ */