#include "ARFFUtils.h"

using namespace arff;

WriteArff::WriteArff(std::string path) : path(path) {
    ofs.open(path);
    isdata = false;
}

WriteArff::~WriteArff() {
    if (ofs.is_open())
        ofs.close();
}

void WriteArff::setRelation(std::string relation) {
    ofs << "@relation ";
    ofs << relation;
    ofs << std::endl << std::endl;
}

void WriteArff::addAttribute(std::string attribute, AttrType type) {
    ofs << "@attribute ";
    ofs << attribute << " ";
    ofs << getAttrTypeString(type, attribute);
    ofs << std::endl;
}

void WriteArff::addData(std::string data) {
    if (!isdata) {
        ofs << std::endl << "@data" << std::endl;
        isdata = true;
    }

    ofs << data << std::endl;
}

void WriteArff::addNominalType(std::string name, std::string values) {
    nominals.insert(std::pair<std::string, std::string>(name, values));
}

std::string WriteArff::getAttrTypeString(AttrType type, std::string nominal) {
    switch (type) {
        case NUMERIC: return "numeric";
        case NOMINAL: return nominals.find(nominal)->second;
        case STRING:  return "string";
        case DATE:    return "date \"yyyy-MM-dd HH:mm:ss\"";
    }
}

void WriteArff::close() {
    ofs.close();
}